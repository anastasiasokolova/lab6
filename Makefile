all: main.o
	gcc -o lab6 main.o status_codes.o bmp_functions.o -lm

main.o: status_codes.o bmp_functions.o main.c
	gcc -c -o main.o main.c

status_codes.o: status_codes.c
	gcc -c -o status_codes.o status_codes.c

bmp_functions.o: status_codes.o bmp_functions.c
	gcc -c -o bmp_functions.o bmp_functions.c

clean:
	rm lab6 *.o