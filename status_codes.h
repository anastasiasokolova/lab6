#ifndef STATUS_CODES_H
#define STATUS_CODES_H

enum read_status
{
    READ_OK = 0,
    READ_TYPE_ERR,
    READ_RESERVED_ERR,
    READ_UNSUPPORTED_FORMAT,
    READ_ERR
};

enum write_status
{
    WRITE_OK = 0,
    WRITE_ERROR
};

const char* get_read_status_message(enum read_status err_code);

const char* get_write_status_message(enum write_status err_code);



#endif // STATUS_CODES_H
