#include "bmp_functions.h"

size_t calculate_padding(int img_width)
{
    size_t padding = 4 - (img_width * 3) % 4;
    return padding == 4 ? 0 : padding;
}


enum read_status readBMP(FILE* f, struct BMP * image)
{
    image->header = (struct bmp_head *)malloc(sizeof(struct bmp_head));
    fread(image->header, sizeof(struct bmp_head), 1, f);
    if (image->header->type != 0x424D && image->header->type != 0x4D42) return READ_TYPE_ERR;
    if (image->header->reserved != 0) return READ_RESERVED_ERR;
    if (image->header->bitCount != 24) return READ_UNSUPPORTED_FORMAT;

    image->width = image->header->width;
    image->height = image->header->height;

    image->size = image->width * image->height;
    image->pxArray = (struct pixel *)malloc(sizeof(struct pixel) * image->size);

    int8_t tr[4] = {0};
    size_t padding = calculate_padding(image->width);
    fseek(f, image->header->offBits, SEEK_SET);
    for (int i = 0; i < image->height; i++)
    {
        if(!fread(&image->pxArray[i*image->width], sizeof(struct pixel), image->width, f)) return READ_ERR;
        if (padding != 0)
        {
            fread(tr, sizeof(int8_t), padding, f);
        }
    }
    return READ_OK;
}

enum write_status writeBMP(struct BMP image, FILE* out)
{
    int8_t tr[4] = {0};
    const size_t pad = calculate_padding(image.width);

    if (!fwrite(image.header, sizeof(struct bmp_head), 1, out)){ printf("hdr"); return WRITE_ERROR;}
    for (int i = 0; i < image.height; i++)
    {
        if (!fwrite(&image.pxArray[i*image.width], sizeof(struct pixel), image.width, out)) { printf("data"); return WRITE_ERROR;}
        if (pad != 0)
        {
            fwrite(tr, sizeof(int8_t), pad, out);
        }
    }
    return WRITE_OK;
}

struct BMP rotate(struct BMP image)
{
    struct BMP newImage = image;
    newImage.width = image.height;
    newImage.height = image.width;
    newImage.header->height = image.width;
    newImage.header->width = image.height;
    newImage.header->fileSize = (newImage.width + calculate_padding(newImage.width)) * newImage.height + 54;

    double x0 = image.width / 2;
    double y0 = image.height / 2;
    double x0_new = y0;
    double y0_new = x0;

    int x_new;
    int y_new;

    for (int y = 0; y < newImage.height; y++)
    {
        for (int x = 0; x < newImage.width; x++)
        {
            x_new = round(x - x0_new + x0);
            y_new = round(y0_new - y + y0);
            newImage.pxArray[y * newImage.width + x] = image.pxArray[y_new * image.width + x_new];
        }
    }

    return newImage;
}
