#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bmp_functions.h"
#include "status_codes.h"

int main(int arg_num, char** args) {
    printf("read...\n");
    if (arg_num < 3) {
        puts("Please write rb file and wb file");
        return 0;
    }

    char* src_file = args[1];
    char* dst_file = args[2];

    FILE *f = fopen(src_file, "rb");
    struct BMP input;
    printf("%s\n", get_read_status_message(readBMP(f, &input)));
    fclose(f);
    FILE *out = fopen(dst_file, "wb");
    printf("rotate...\n");
    struct BMP output = rotate(input);
    printf("write...\n");
    printf("%s\n", get_write_status_message(writeBMP(output, out)));
    fclose(out);
    return 0;
}

