#include "status_codes.h"

static const char* read_status_msg[] =
{
        [READ_OK] = "OK",
        [READ_TYPE_ERR] = "Type error",
        [READ_RESERVED_ERR] = "Incorrect image",
        [READ_UNSUPPORTED_FORMAT] = "Unsupported format",
        [READ_ERR] = "Read error"
};

static const char* write_status_msg[] =
{
        [WRITE_OK] = "OK",
        [WRITE_ERROR] = "Write error"
};

const char* get_read_status_message(enum read_status err_code)
{
    return read_status_msg[err_code];
}

const char* get_write_status_message(enum write_status err_code)
{
    return write_status_msg[err_code];
}
